using System;
using System.Linq;
using System.Text.RegularExpressions;
using Xunit;

namespace Tree.Tests
{
    public class TreeNodeTest : TreeBase<TreeNodeTest>
    {
    }

    public class TreeBase_VariousTests
    {
        public TreeBase_VariousTests()
        {            
        }

        public TreeNodeTest GetTestData()
        {
            TreeNodeTest root = new TreeNodeTest { Key = "1" };
            root.AddChild(new TreeNodeTest { Key = "1.1" });
            root.AddChild(new TreeNodeTest { Key = "1.2" });
            root.AddChild(new TreeNodeTest { Key = "1.3" });

            var node_1_1 = root.Children[0];
            node_1_1.AddChild(new TreeNodeTest { Key = "1.1.1" });
            node_1_1.AddChild(new TreeNodeTest { Key = "1.1.2" });

            var node_1_1_1 = node_1_1.Children[0];
            node_1_1_1.AddChild(new TreeNodeTest { Key = "1.1.1.1" });
            node_1_1_1.AddChild(new TreeNodeTest { Key = "1.1.1.2" });
            node_1_1_1.AddChild(new TreeNodeTest { Key = "1.1.1.3" });

            var node_1_1_1_1 = node_1_1_1.Children[0];
            node_1_1_1_1.AddChild(new TreeNodeTest { Key = "1.1.1.1.1" });

            var node_1_1_1_2 = node_1_1_1.Children[1];
            node_1_1_1_2.AddChild(new TreeNodeTest { Key = "1.1.1.2.1" });

            var node_1_1_1_3 = node_1_1_1.Children[2];
            node_1_1_1_3.AddChild(new TreeNodeTest { Key = "1.1.1.3.1" });
            node_1_1_1_3.AddChild(new TreeNodeTest { Key = "1.1.1.3.2" });
            node_1_1_1_3.AddChild(new TreeNodeTest { Key = "1.1.1.3.3" });
            
            var node_1_1_1_3_1 = node_1_1_1_3.Children[0];
            node_1_1_1_3_1.AddChild(new TreeNodeTest { Key = "1.1.1.3.1.1" });
            node_1_1_1_3_1.AddChild(new TreeNodeTest { Key = "1.1.1.3.1.2" });

            var node_1_1_1_3_3 = node_1_1_1_3.Children[2];
            node_1_1_1_3_3.AddChild(new TreeNodeTest { Key = "1.1.1.3.3.1" });
            node_1_1_1_3_3.AddChild(new TreeNodeTest { Key = "1.1.1.3.3.2" });

            return root;
        }

        [Fact]
        public void TraversalTest()
        {
            TreeNodeTest root = GetTestData();
            int count = 0;
            root.Traverse(x => count++);
            Assert.True(count == 18);
        }

        [Fact]
        public void FindDepthFiveNodeAndCheckDepth()
        {
            TreeNodeTest root = GetTestData();
            var node = root.FindFirst(x => x.Key.Equals("1.1.1.3.3.2"));
            Assert.NotNull(node);
            Assert.True(node.GetDepth() == 5);
        }

        [Fact]
        public void FindAllPath_1_1_1_3()
        {
            // search all
            TreeNodeTest root = GetTestData();
            var regex = new Regex(@"^1\.1\.1\.3\.\d$");
            var nodes = root.FindAll(x => regex.IsMatch(x.Key));
            
            Assert.True(nodes.Count == 3);
            
            // path
            var path = nodes[0].GetPath();
            var node = root.FindFromPath(path);
            Assert.True(node == nodes[0]);

            // top
            Assert.True(root == node.FindTop());

            // ancestors
        }

        [Fact]
        public void GetAncestorsTest()
        {
            TreeNodeTest root = GetTestData();
            var node = root.FindFirst(x => x.Key.Equals("1.1.1.3.3.2"));
            var ancestors = node.GetAncestors();
            var depth = node.GetDepth() - 1;
            var parent = node.Parent;
            foreach (var item in ancestors)
            {
                Assert.True(depth == item.depth);
                depth--;

                Assert.True(item.ancestor == parent);
                parent = parent.Parent;
            }

            var node2 = root.FindFirst(x => x.Key.Equals("1.1.1.3.3.1"));

            Assert.True(node2.Parent == node.Parent, "Sibling parents are different");
            var ancestors2 = node2.GetAncestors();

            Assert.True(ancestors.Zip(ancestors2, (x, y) => new { first = x, second = y })
                .All(x => x.first.ancestor == x.second.ancestor), "Sibling ancestors are different");
        }

        [Fact]
        public void CloneTest()
        {
            TreeNodeTest root = GetTestData();
            var node = root.FindFirst(x => x.Key.Equals("1.1.1.3.3.2"));
            var path = node.GetPath();
            var copy = root.CloneDownwards();
            var nodeCopy = copy.FindFromPath(path);

            Assert.True(nodeCopy.Key.Equals(node.Key));

            node.Key = "original";
            nodeCopy.Key = "copy";

            // test that they are different objects
            Assert.True(node.Key.Equals("original"));
            Assert.True(nodeCopy.Key.Equals("copy"));

            // test children lists are different objects
            node = node.Parent;
            node.Children.Clear();

            Assert.True(nodeCopy.Parent.Children.Count > 0);
        }
    }
}
