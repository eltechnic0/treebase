using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;

namespace System.Collections.Generic
{
    public static class CollectionExtensions
    {
        public static OrderedDictionary<T1, T2> ToOrderedDictionary<T1, T2>(
            this IEnumerable<T2> source, Func<T2, T1> keySelector)
        {
            var dict = new OrderedDictionary<T1, T2>();
            foreach (var item in source)
                dict[keySelector(item)] = item;
            return dict;
        }

        public static KeyValuePair<T1, T2> Last<T1, T2>(this OrderedDictionary<T1, T2> source)
        {
            return source.GetItem(source.Count - 1);
        }
    }
}