using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Tree
{
    public interface ITree
    {
        string Key { get; set; }
        ITree Parent { get; }
        IReadOnlyList<ITree> Children { get; }

        void AddChild(ITree node);
        bool RemoveChild(ITree node);
        void Traverse(Action<ITree> fun);
        void Traverse(Action<ITree, object> fun, object param);
        void TraverseUp(Action<ITree> fun);
        void TraverseUp(Action<ITree, object> fun, object param);
        ITree FindTop();
        ITree FindFirst(Func<ITree, bool> predicate);
        IReadOnlyList<ITree> FindAll(Func<ITree, bool> predicate);
        int GetDepth();
    }

    public abstract class TreeBase<T> : ITree where T : TreeBase<T>
    {
        public string Key { get; set; }
        public T Parent { get; set; }
        public List<T> Children { get; set; } = new List<T>();
        
        #region ITree

        ITree ITree.Parent => Parent;
        IReadOnlyList<ITree> ITree.Children => Children;

        public bool RemoveChild(ITree node) => RemoveChild(node);

        public void AddChild(ITree node) => AddChild(node);

        IReadOnlyList<ITree> ITree.FindAll(Func<ITree, bool> predicate) => FindAll(predicate);

        ITree ITree.FindFirst(Func<ITree, bool> predicate) => FindFirst(predicate);

        ITree ITree.FindTop() => FindTop();

        #endregion

        public bool RemoveChild(T node) => Children.Remove(node);

        public void AddChild(T node)
        {
            node.Parent = (T)this;
            Children.Add(node);
        }

        public T2 CopyStructure<T2>(Func<T, T2> map) where T2 : TreeBase<T2>
        {            
            void CopyStructureInternal(T source, T2 target)
            {
                foreach (var item in source.Children)
                {
                    var other = map(item);
                    target.Children.Add(other);
                    CopyStructureInternal(item, other);
                }
            }

            var top = map((T)this);
            CopyStructureInternal((T)this, top);
            return top;
        }

        public T FindFirst(Func<ITree, bool> predicate)
        {
            if (predicate(this) == true)
                return (T)this;
            T res = null;
            foreach (var item in Children)
            {
                res = item.FindFirst(predicate);
                if (res != null)
                    return res;
            }
            return null;
        }

        public List<T> FindAll(Func<ITree, bool> predicate)
        {
            List<T> result = new List<T>();

            void FindAllInternal(T node)
            {
                if (predicate(node) == true)
                    result.Add(node);
                foreach (var item in node.Children)
                    FindAllInternal(item);
            }

            FindAllInternal((T)this);
            return result;
        }

        public T FindTop()
        {
            var node = this;
            while (node.Parent != null)
                node = node.Parent;
            return (T)node;
        }

        public void Traverse(Action<ITree> fun)
        {
            fun(this);
            foreach (var item in Children)
                item.Traverse(fun);
        }

        public void Traverse(Action<ITree, object> fun, object param)
        {
            fun(this, param);
            foreach (var item in Children)
                item.Traverse(fun, param);
        }

        public void TraverseUp(Action<ITree> fun)
        {
            var node = this;
            fun(node);
            while (node.Parent != null)
            {
                node = node.Parent;
                fun(node);
            }
        }

        public void TraverseUp(Action<ITree, object> fun, object param)
        {
            var node = this;
            fun(node, param);
            while (node.Parent != null)
            {
                node = node.Parent;
                fun(node, param);
            }
        }

        public int GetDepth()
        {
            int depth = 0;
            var node = this;
            while (node.Parent != null)
            {
                node = node.Parent;
                depth++;
            }
            return depth;
        }

        public T CloneDownwards()
        {
            var res = ShallowClone();
            res.Parent = null;
            res.Traverse(n =>
            {
                var node = n as T;
                var children = node.Children;
                node.Children = new List<T>();
                foreach (var item in children)
                {
                    var newChild = item.ShallowClone();
                    newChild.Parent = node;
                    node.Children.Add(newChild);
                }
            });
            return res;
        }

        public virtual T ShallowClone()
        {
            var res = MemberwiseClone() as T;
            res.Children = new List<T>(Children);
            return res;
        }

        public List<int> GetPath()
        {
            var node = this as T;
            var child = node as T;
            var path = new List<int>();

            while (node.Parent != null)
            {
                node = node.Parent;
                path.Add(node.Children.IndexOf(child));
                child = node;
            }
            path.Reverse();

            return path;
        }

        public T FindFromPath(List<int> path)
        {
            T res = this as T;
            foreach (var item in path)
                res = res.Children[item];
            return res;
        }

        // one pass up, then another iteration to compute depths
        public List<(T ancestor, int depth)> GetAncestors1()
        {
            int depth = 0;
            var node = (T)this;
            var ancestors = new List<(T ancestor, int depth)>();
            while (node.Parent != null)
            {
                node = node.Parent;
                depth++;
                ancestors.Add((node, depth));
            }
            int maxDepth = ancestors.Last().depth;
            for (int i = 0; i < ancestors.Count; i++)
            {
                var (ancestor, level) = ancestors[i];
                ancestors[i] = (ancestor, maxDepth - level);
            }
            return ancestors;
        }

        // compute depth, then work upwards
        // TODO: compare the two implementations for performance
        public List<(T ancestor, int depth)> GetAncestors()
        {
            int depth = this.GetDepth();
            var node = (T)this;
            var ancestors = new List<(T ancestor, int depth)>();
            while (node.Parent != null)
            {
                node = node.Parent;
                depth--;
                ancestors.Add((node, depth));
            }
            return ancestors;
        }

        public (T ancestor, int depth) FindSharedAncestor(T other, OrderedDictionary<T, (T ancestor, int depth)> ancestors = null)
        {
            if (this == other)
                return (this.Parent, this.GetDepth());

            // get other node's ancestors
            ancestors = ancestors ?? other.GetAncestors().ToOrderedDictionary(x => x.ancestor);

            // check this node's ancestors for a match
            bool found = false;
            var node = (T)this;
            while (node.Parent != null)
            {
                node = node.Parent;
                if (ancestors.ContainsKey(node))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
                return (null, 0);

            return ancestors[node];
        }

        // assuming the dict is ordered from deepest to root (last will be depth == 0)
        public static (T ancestor, int depth) FindSharedAncestor(
            OrderedDictionary<T, (T ancestor, int depth)> thisAncestors, 
            OrderedDictionary<T, (T ancestor, int depth)> otherAncestors)
        {
            bool found = false;
            T node = null;
            var otherLast = otherAncestors[0].ancestor;
            for (int i = 0; i < thisAncestors.Count; i++)
            {
                var thisCurrent = thisAncestors[i].ancestor;
                if (otherLast == thisCurrent)
                {
                    found = true;
                    node = thisCurrent;
                    break;
                }
            }

            if (!found)
                return (null, 0);

            return thisAncestors[node];
        }
    }

    public abstract class TreeNode : TreeBase<TreeNode>
    {
        public enum NodeTypeEnum { Disjunctive, Diagnosed, EndPoint }

        public abstract NodeTypeEnum NodeType { get; }
        public string Description { get; set; }
        public double Lambda { get; set; }
        public double Fit { get => Lambda * 1E9; set => Lambda = value / 1E9; }
    }

    public class LinkedTree : TreeBase<LinkedTree>
    {
        public TreeNode Node { get; set; }
        public TreeNode ParentLink { get; set; }
    }

    public class TableTree<T> where T : TreeBase<T>
    {
        private Dictionary<string, T> _nodes = new Dictionary<string, T>();

        public T this[string key] { get => _nodes[key]; }

        public ICollection<string> Keys => _nodes.Keys;

        public ICollection<T> Values => _nodes.Values;

        public int Count => _nodes.Count;

        public void Add(T node) => _nodes.Add(node.Key, node);

        public void Clear() => _nodes.Clear();

        public bool Contains(T item) => _nodes.ContainsKey(item.Key);

        public bool Contains(string key) => _nodes.ContainsKey(key);

        public bool Remove(string key) => _nodes.Remove(key);

        public bool Remove(T item) => _nodes.Remove(item.Key);

        public bool TryGetValue(string key, out T value)
        {
            if (Contains(key))
            {
                value = _nodes[key];
                return true;
            }
            value = null;
            return false;
        }

    }
}